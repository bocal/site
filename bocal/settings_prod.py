import os
from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
# For production, generate a fresh one, eg. with
#     pwgen -sy 60 1
SECRET_KEY = 'CHANGEMEQUICKLY'  # FIXME

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['localhost',
                 ]  # FIXME: add your domain name(s) here.

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': { # FIXME add real settings
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '',  # DB name
        'USER': '',  # DB user
        'PASSWORD': '',  # user's password
        'HOST': 'localhost',  # DB host -- change if DB is not local
        'PORT': '5432',  # DB port -- 5432 is the default port for postgres
    },

    # Alternatively, use sqlite3 (if you don't really have a choice…)
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    # }
}

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/
LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Europe/Paris'

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Paths

STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')

# Cas
CAS_SERVER_URL = 'https://example.com/'  # FIXME
CAS_VERIFY_URL = 'https://example.com/'  # FIXME
CAS_VERSION = 'CAS_2_SAML_1_0'  # FIXME
CAS_IGNORE_REFERER = True
CAS_FORCE_CHANGE_USERNAME_CASE = 'lower'
CAS_REDIRECT_URL = '/'
CAS_EMAIL_FORMAT = "%s@clipper.ens.fr"  # FIXME
CAS_LOGOUT_COMPLETELY = False

# Auth
RHOSTS_PATH = ''  # FIXME (path to BOcal's .rhosts)

import os
from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
# For production, generate a fresh one, eg. with
#     pwgen -sy 60 1
SECRET_KEY = 'k340m-_mw#i#up8ajv9$$=$tgpji3f3j!jafj2+ken*@wo9u0%'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/
LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Europe/Paris'

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Paths

STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')

# Cas
CAS_SERVER_URL = 'https://cas.eleves.ens.fr/'
CAS_VERIFY_URL = 'https://cas.eleves.ens.fr/'
CAS_VERSION = 'CAS_2_SAML_1_0'
CAS_IGNORE_REFERER = True
CAS_FORCE_CHANGE_USERNAME_CASE = 'lower'
CAS_REDIRECT_URL = '/'
CAS_EMAIL_FORMAT = "%s@clipper.ens.fr"
CAS_LOGOUT_COMPLETELY = False

# Auth
RHOSTS_PATH = 'rhosts_dev'

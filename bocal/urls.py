from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required
import django.contrib.auth.views as dj_auth_views

import mainsite.urls
import bocal_auth.views as auth_views
from bocal_auth.rhosts import forceReevalRhosts
import markdownx.urls
import api.urls

import django_cas_ng.views


# Force the user to login through the custom login page
admin.site.login = login_required(forceReevalRhosts(admin.site.login))

cas_patterns = [
    path("login/", django_cas_ng.views.LoginView.as_view(), name="cas_ng_login"),
    path("logout/", django_cas_ng.views.LogoutView.as_view(), name="cas_ng_logout"),
    path(
        "callback/",
        django_cas_ng.views.CallbackView.as_view(),
        name="cas_ng_proxy_callback",
    ),
]

accounts_patterns = [
    path("cas/", include(cas_patterns)),
    path("login/", auth_views.login, name="login"),
    path("logout/", auth_views.logout, name="logout"),
    path("password_login/", dj_auth_views.LoginView.as_view(), name="password_login"),
]

urlpatterns = [
    path("admin/", admin.site.urls),
    path("markdownx/", include(markdownx.urls)),
    path("api/", include(api.urls)),
    path("accounts/", include(accounts_patterns)),
    path("", include(mainsite.urls)),
]

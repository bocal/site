# Site du BOcal

L'ancien site du BOcal (en 2017) est de plus en plus horrible à gérer, de plus
en plus moche, de plus en plus patché salement. Il est temps de passer à autre
chose.

## Contributeurs

S'il y a besoin de contacter un·e vieux·ille ayant bossé sur ce site, les noms
et adresses de contact (quoique les clippers seront sûrement invalides) se
trouvent dans les « commits » du projet. Au pire, un·e info saura trouver ça :)

## Installer

Récupérer le site :
```bash
git clone https://git.eleves.ens.fr/bocal/site bocal-site
cd bocal-site
```

Créer un « virtualenv » (paquets python séparés pour le projet) et installer
les dépendances :
```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

Configurer le site, **uniquement si c'est pour déployer le site en
production** :
```bash
cd bocal
ln -sf settings_prod.py settings.py
```

Ensuite, toujours pour la mise en production, **éditez settings.py** et changez
au minimum tous les endroits où des `FIXME` sont écrits. En particulier,
**générez une secret key aléatoire et forte**, par exemple avec
```bash
pwgen -sy 60 1
```

Dans tous les cas — pour le dev ou la production —, créez les structures
nécessaires en base de données :
```bash
cd ..  # Pour revenir dans bocal-site
./manage.py migrate
./manage.py loaddata bocal_group
```

À partir de là, ça tourne :) Il ne reste plus qu'à faire tourner soit avec un
`./manage.py runserver` si c'est pour développer dessus/faire des tests, soit
avec un `nginx` et un `gunicorn` ou autre WSGI.

### Changer le CSS

**Ne modifiez pas les fichiers `.css`**. Pour modifier l'apparence du site :
- installez [compass](https://compass-style.org/)
- placez-vous dans `mainsite/static', et lancez `compass watch` en tâche de fond dans un terminal
- modifiez les fichiers `.scss` dans `mainsite/static/sass`. `compass` les compilera à chaque fois que vous les enregistrerez

## Utilisation

Côté BOcaleu·ses·x, quelques opérations seront nécessaires/utiles.

### Interface d'administration

Elle est disponible à l'adresse `/admin` sur le site. Par exemple, si le site
tourne sur `www.cof.ens.fr/bocal`, l'interface d'admin se trouve sur
`www.cof.ens.fr/bocal/admin`.

### Publier un BOcal

[TODO : créer des scripts automagiques à lancer depuis bocal@sas.eleves]

Se connecter à l'interface d'admin, `publications`, `ajouter`. Si le numéro est
marqué comme « spécial » ici, il sera placé dans une rubrique à part.

### Nouvelle année scolaire

Pour ajouter une nouvelle année scolaire à gauche de l'écran, il faut créer une
« publication year » (interface d'admin). L'année à donner est l'année civile
du début de l'année (*eg.* pour 2017-2018, c'est 2017). L'accroche est le court
(ou pas) texte en haut de la page de l'année.

**Important :** l'année n'apparaîtra qu'une fois qu'un premier BOcal y aura été
ajouté ! (Les BOcals sont ajoutés selon leur date de parution).

**Tout aussi important :** si un BOcal est publié, mais que son année scolaire
n'apparaît pas dans les « publication years », il ne sera pas trouvable sur le
site !

#!/bin/bash 

mv $1.html $1.html.backup
name=$(head -n 1 $1.htm)
if [[ $name == '' ]]
then
	name=$(cat ./includes/header.html | grep -Po '\${title "\K[^"]*')
fi
header=$(cat ./includes/header.html | sed "s/\${title[^}]*}/$name/")
echo "$header" > $1.html
echo "`cat ./includes/menu.html`" >> $1.html
echo "`tail -n +2 $1.htm`" >> $1.html
echo "`cat ./includes/footer.html`" >> $1.html

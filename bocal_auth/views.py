from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required

from urllib.parse import quote as urlquote


def login(req):
    if req.user.is_authenticated:
        return redirect("homepage")

    if req.method == "GET":
        reqDict = req.GET
    elif req.method == "POST":
        reqDict = req.POST
    if "next" in reqDict:
        nextUrl = reqDict["next"]
        context = {
            "pass_url": "{}?next={}".format(
                reverse("password_login"), urlquote(nextUrl, safe="")
            ),
            "cas_url": "{}?next={}".format(
                reverse("cas_ng_login"), urlquote(nextUrl, safe="")
            ),
        }
    else:
        context = {
            "pass_url": reverse("password_login"),
            "cas_url": reverse("cas_ng_login"),
        }

    return render(req, "mainsite/login.html", context=context)


@login_required
def logout(req):
    CAS_BACKEND_NAME = "django_cas_ng.backends.CASBackend"
    if req.session["_auth_user_backend"] != CAS_BACKEND_NAME:
        auth_logout(req)
        return redirect("homepage")
    return redirect("cas_ng_logout")

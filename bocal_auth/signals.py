from django.dispatch import receiver
from django_cas_ng.signals import cas_user_authenticated, cas_user_logout
from . import rhosts


@receiver(cas_user_authenticated)
def onCasLogin(sender, user, **kwargs):
    ''' Called upon login of a user through CAS '''
    rhosts.evalRhostsPrivileges(user)


@receiver(cas_user_logout)
def onCasLogout(sender, user, **kwargs):
    ''' Strip the user from their privileges — in case something goes wrong
    during the next authentication '''
    rhosts.logout(user)

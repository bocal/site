from django_cas_ng.backends import CASBackend
from .models import CasUser


class BOcalCASBackend(CASBackend):
    # Partly from Robin Champenois's "ExperiENS". Thanks!
    def clean_username(self, username):
        return username.lower().strip()

    def configure_user(self, user):
        casUser = CasUser(user=user)
        casUser.save()
        return user

from django.db import models
from django.contrib.auth.models import User


class CasUser(models.Model):
    ''' Describes a Django user that was created through CAS '''

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True)

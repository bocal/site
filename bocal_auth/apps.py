from django.apps import AppConfig


class BocalAuthConfig(AppConfig):
    name = 'bocal_auth'

    def ready(self):
        from . import signals

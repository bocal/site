from django.conf.urls import url
from . import views

urlpatterns = [
    url(r"^$", views.HomeView.as_view(), name="homepage"),
    url(r"^robots.txt$", views.robots_view, name="robots"),
    url(r"^ecrire$", views.WriteArticleView.as_view(), name="write_article"),
    url(
        r"^speciaux/",
        views.SpecialPublicationsView.as_view(),
        name="special_publications",
    ),
    url(
        r"^(?P<year>\d{4})-(?P<nYear>\d{4})/",
        views.YearView.as_view(),
        name="year_view",
    ),
    url(r"^latest$", views.latestPublication, name="latestPublication"),
]

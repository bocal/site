from django.shortcuts import get_object_or_404, redirect
from django.views.generic import TemplateView
from django.http import Http404, HttpResponse

from mainsite.models import Publication, PublicationYear, SiteConfiguration


def robots_view(request):
    """ Robots.txt view """
    body = "User-Agent: *\nDisallow: /\nAllow: /$\n"
    return HttpResponse(body, content_type="text/plain")


class HomeView(TemplateView):
    """ Website's homepage """

    template_name = "mainsite/homepage.html"


class WriteArticleView(TemplateView):
    """ Tell the readers how they can contribute to the BOcal """

    template_name = "mainsite/write_article.html"


class PublicationListView(TemplateView):
    """ Display a list of publications (generic class).

    Reimplement `get_publications` (called with the url template args in a
    place where you can Http404) in subclasses to get it working. """

    template_name = "mainsite/publications_list_view.html"

    def initView(self):
        """ Cannot be __init__, we don't have **kwargs there """
        pass

    def get_context_data(self, **kwargs):
        self.initView(**kwargs)

        context = super(PublicationListView, self).get_context_data(**kwargs)
        context.update(self.additional_context(**kwargs))

        publications = self.get_publications(**kwargs)

        if len(publications) == 0:
            raise Http404
        context["publications"] = publications

        return context


class YearView(PublicationListView):
    """ Display a year worth of BOcals """

    def initView(self, year, nYear):
        try:
            year, nYear = int(year), int(nYear)
        except ValueError:
            raise Http404
        if year + 1 != nYear:
            raise Http404
        self.year = year

        self.pubYear = get_object_or_404(PublicationYear, startYear=year)

    def additional_context(self, year, nYear):
        return {
            "intro_text": self.pubYear.descr,
            "is_year_view": True,
            "year_range": self.pubYear.prettyName,
        }

    def get_publications(self, year, nYear):
        return self.pubYear.publis()


class SpecialPublicationsView(PublicationListView):
    """ Display the list of special publications """

    def additional_context(self):
        siteConf = SiteConfiguration.get_solo()
        return {
            "intro_text": siteConf.specialPublisDescr,
            "is_year_view": False,
            "list_title": "Numéros spéciaux",
        }

    def get_publications(self):
        publications = Publication.objects.filter(is_special=True).order_by("-date")
        return publications


def latestPublication(req):
    """ Redirects to the latest standard publication """
    latestPubli = Publication.latest()
    return redirect(latestPubli.url)

""" Context processors """

from . import models


def sidebar_years(req):
    avail_years = models.PublicationYear.objects.all()
    publi_years = [year for year in avail_years if year.publis().count() > 0]

    num_special_publications = models.Publication.objects\
        .filter(is_special=True).count()

    return {
        'publication_years': publi_years,
        'has_special_publications': num_special_publications > 0,
    }

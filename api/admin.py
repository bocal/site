from django.contrib import admin
from . import models


@admin.register(models.ApiKey)
class ApiKeyAdmin(admin.ModelAdmin):
    list_display = ['name', 'last_used', 'displayValue']
    readonly_fields = ['keyId', 'key', 'last_used', 'displayValue']

    def save_model(self, request, obj, form, change):
        if not change:
            obj.initialFill()
        super(ApiKeyAdmin, self).save_model(request, obj, form, change)

from django.conf.urls import url
from . import views

app_name = 'manisite'

urlpatterns = [
    url(r'^publish$', views.publishApiView),
]
